# 50 States Interactive Map
# 10/08/14
# Arthur He, Jack Morris, Katherine Van Kirk

# - - - - - - - - - - - - - - > 

from Tkinter import *

from random import random

# - - - - - - - - - - - - - - >


def main():
   #
   root = Tk()
   #
   w = 600
   h = 400
   x = 1
   #
   canvas = Canvas(root, width=w, height=h)
   canvas.pack()
   #
   canvas.create_line(0, 100, 200, 0, fill="red", dash=(4, 4))
   #
   while True:
      #
      draw(w,h,x,canvas,root)
      x += 1
      #
   #
   root.mainloop()
   #

def draw(w,h,x,canvas,root):
   #
   print "loop ", x
   #
   r1 = (int)(random() * w)
   r2 = (int)(random() * h)
   r3 = (int)(random() * (w - r1))
   r4 = (int)(random() * (h - r2))
   #
   canvas.create_rectangle(r1, r2, r3, r4, fill="blue")
   canvas.after(1000)
   canvas.update()
   #

main()
