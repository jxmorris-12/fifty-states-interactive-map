# 50 States Interactive Map
# 10/08/14
# Arthur He, Jack Morris, Katherine Van Kirk

# - - - - - - - - - - - - - - > 

from Tkinter import *
import time
import thread
from random import random

# - - - - - - - - - - - - - - >
canvas = None
panning = False
states_array = {}
canvas = None
root = None
w = 0
h = 0
p = 0.05
orig_minlat = 0
orig_maxlat = 0
orig_minlon = 0
orig_maxlon = 0
minlat = 0
maxlat = 0
minlon = 0
maxlon = 0

class State():
   #
   nbrs = []
   name = "empty"
   my_id = ""
   orig_coords = []
   new_coords = []
   #
   def __init__(self,name,orig_coords): #initialize state
      self.name = name
      self.orig_coords = orig_coords
      self.new_coords  = [0] * len(orig_coords)
      self.nbrs = []
   #
   def get_name(self):
      return self.name
   #
   def get_id(self):
      return self.my_id
   #
   def get_polygon(self):
      return self.points
   #
   def outline(self,canvas): #draws state to canvas
      canvas.itemconfig(self.my_id,outline="blue",fill="white") #draw each state
   #
   def highlight(self,canvas): #draws state to canvas
      canvas.itemconfig(self.my_id,outline="blue",fill="yellow",stipple="gray25") #fill each state
      self.highlight_neighbors(canvas)
   # 
   def shade(self,canvas):
      canvas.itemconfig(self.my_id,outline="blue",fill="gray") #fill each state
   #
   def make_nbrs(self, array):
      #
      global states_array
      #
      for n in array:
      #
         #
         # print "appending nbr",states_array[n].get_name(),"to state",self.name
         #
         self.nbrs.append( states_array[n] )
      #
   #
   def highlight_neighbors(self,canvas):
      #
      for n in self.nbrs:
         n.shade(canvas)
      #
      # print
      #
   #
   def process(self,canvas):
      #
      L = len(self.orig_coords)
      #
      canvas.delete(self.my_id)
      #
      for x in xrange(L):
         #
         global w,h,minlon,maxlon,minlat,maxlat
         #
         lat_range = maxlat - minlat # eq. to height
         lon_range = maxlon - minlon # eq. to width
         #
         lat = self.orig_coords[x][0]
         lon = self.orig_coords[x][1]
         #
         new_lon = int((lon - minlon) * float(w) / lon_range)
         new_lat = int((lat - minlat) * float(h) / lat_range)
         #
         self.new_coords[x] = (w-new_lon, new_lat)
      #
      self.my_id = canvas.create_polygon(self.new_coords,outline="blue",fill="white")
      #

def read_data(s):
   data = {}
   #
   with open(s,'r') as raw_data:
      data = eval(raw_data.read())
   return data
   #

def min_lat(data,keys):
   m = data[keys[0]][0][0]
   for key in keys:
      L = len(data[key])
      for x in xrange(L):
         l = data[key][x][0]
         if(m < l): m = l
   return m

def min_lon(data,keys):
   m = data[keys[0]][0][1]
   for key in keys:
      L = len(data[key])
      for x in xrange(L):
         l = data[key][x][1]
         if(m < l): m = l
   return m

def max_lat(data,keys):
   m = data[keys[0]][0][0]
   for key in keys:
      L = len(data[key])
      for x in xrange(L):
         l = data[key][x][0]
         if(m > l): m = l
   return m
#
def max_lon(data,keys):
   m = data[keys[0]][0][1]
   for key in keys:
      L = len(data[key])
      for x in xrange(L):
         l = data[key][x][1]
         if(m > l): m = l
   return m
   
def make_nbrs(s): # return void
   data = {}
   #
   with open(s,'r') as raw_data:
      data = eval(raw_data.read())
   #
   # print "data:",data
   #
   for state in data:
      states_array[state].make_nbrs( data[state] )
   #

def click(event):
   #
   global w,h,minlon,maxlon,minlat,maxlat,canvas,p
   #
   lat_range = maxlat - minlat # eq. to height
   lon_range = maxlon - minlon # eq. to width
   #
   deltalat = p * lat_range
   deltalon = p * lon_range
   #
   minlat += deltalat
   maxlat -= deltalat
   #
   minlon += deltalon
   maxlon -= deltalon
   #
   for state in states_array:
      states_array[state].process(canvas)
   #
   motion(event)
   #

def click2(event):
   #
   global w,h,minlon,maxlon,minlat,maxlat,canvas,p
   #
   lat_range = maxlat - minlat # eq. to height
   lon_range = maxlon - minlon # eq. to width
   #
   deltalat = p * lat_range
   deltalon = p * lon_range
   #
   minlat -= deltalat
   maxlat += deltalat
   #
   minlon -= deltalon
   maxlon += deltalon
   #
   for state in states_array:
      states_array[state].process(canvas)
   #
   motion(event)
   #

def motion(event):
   #
   global states_array,canvas,root,w,h,p,maxlat,minlat,maxlon,minlon
   #
   x, y = event.x, event.y
   #
   lat_range = maxlat - minlat # eq. to height
   lon_range = maxlon - minlon # eq. to width
   #
   if(x < p * w): #pan left
      minlon += p * lon_range
      maxlon += p * lon_range
   #
   elif(x > (1-p) * w and x < w): #pan right
      minlon -= p * lon_range
      maxlon -= p * lon_range
   #
   if(y < p * h): #pan up
      minlat -= p * lat_range
      maxlat -= p * lat_range
   #
   elif(y > (1-p) * h and y < h): #pan down
      minlat += p * lat_range
      maxlat += p * lat_range
   #
   # fill closest neighbor
   #
   for state in states_array:
      #
      states_array[state].process(canvas)
      #
   #
   closest_polygon_id = canvas.find_closest(x,y)[0]
   #
   for state in states_array:
      #
      states_array[state].outline(canvas)
      #
   #
   for state in states_array:
      #
      if(int(states_array[state].get_id()) == int(closest_polygon_id)):
         #
         states_array[state].highlight(canvas)
         #
      #
   canvas.update()
   #

def main():
   #
   # LAT_RANGE -24.9918 LON_RANGE -58.0042
   #
   global w,h,minlon,maxlon,minlat,maxlat,states_array,canvas,root
   #
   root = Tk()
   #
   w = 1000
   h = w * 0.6
   #
   f = Frame(root.master)
   f.pack(fill=BOTH,expand=YES)
   canvas = Canvas(root, width=w, height=h)
   canvas.pack(fill=BOTH,expand=YES)
   #
   canvas.create_rectangle(0,0,w,h,fill="gray") #background
   #
   point_data = read_data("edited2.txt")
   #
   keys = point_data.keys()
   #
   orig_minlat = min_lat(point_data,keys)
   orig_maxlat = max_lat(point_data,keys)
   orig_minlon = min_lon(point_data,keys)
   orig_maxlon = max_lon(point_data,keys)
   minlat = min_lat(point_data,keys)
   maxlat = max_lat(point_data,keys)
   minlon = min_lon(point_data,keys)
   maxlon = max_lon(point_data,keys)
   #
   for x in xrange(len(keys)):
      #
      state_name = keys[x]
      points = point_data[state_name]
      #
      s = State(state_name,points)
      #
      s.process(canvas)
      #
      states_array[state_name] = s
      #
   #
   make_nbrs("neighbors.txt")
   #
   root.bind("<Motion>", motion)
   root.bind("<Button-1>", click)
   root.bind("<Button-2>", click2)
   root.mainloop()
   #


main()

#
# end of file
#